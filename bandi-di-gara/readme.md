# Invio di bandi di gara tramite l'esecuzione dello script `invia-bandi.php`

### Formato e specifiche del file csv
Il file CSV deve essere in UTF-8 e deve avere come delimitatore di campo il punto e virgola `;` e come enclosure il doppio apice `"`

Il file CSV deve avere le seguenti intestazioni:

- `cig`
- `codice_fiscale_piva_proponente`
- `denominazione_proponente`
- `data_inizio`
- `data_ultimazione`
- `oggetto`
- `importo_gara`
- `importo_aggiudicazione`
- `importo_somme_liquidate`
- `scelta_contraente`
- `invitati.ragione_sociale`
- `invitati.identificativo_fiscale_estero`
- `invitati.codice_fiscale`
- `invitati.id_gruppo`
- `invitati.ruolo`
- `partecipanti`
- `aggiudicatari`


L'ordine delle colonne non influisce sul corretto funzionamento dello script.

Ogni riga del file rappresenta un invitato alla gara e tutte le informazioni relative al bando (`cig`, `codice_fiscale_piva_proponente`, `denominazione_proponente`, `data_inizio`, `data_ultimazione`, `oggetto`, `importo_gara`, `importo_aggiudicazione`, `importo_somme_liquidate`, `scelta_contraente`) che vengono perciò replicate in ciascuna riga.

Il valore del campo `partecipanti` deve essere `2` nel caso l'invitato sia compreso tra i partecipanti, `1` nel caso l'invitato sia escluso dai partecipanti

Il valore del campo `aggiudicatari` deve essere `2` nel caso l'invitato sia compreso tra i aggiudicatari, `1` nel caso l'invitato sia escluso dai aggiudicatari

Il formato della data è giorno/mese/anno, ad esempio per il 5 marzo del 2023: 05/03/2023

Il formato della valuta è con la virgola e senza punti: ad esempio 1250,50 e non 1.250,50

### Esecuzione
Lo script è eseguibile attraverso docker eseguendo il comando 

```docker container run --rm -v $(pwd):/app/ php:7.4-cli php /app/invia-bandi.php -f esempio.csv -t https://example.com/api/openapi/bandi-di-gara/2023 -u admin -p 123456```

Per visualizzare le opzioni dello script agiungere il parametro `-h`

I parametri attesi sono:

- `-f`   percorso al file csv (relativo al percorso dello script)
- `-t`   endpoint api (esempio `https://bugliano-qa.localtest.me/api/openapi/bandi-di-gara/2023`)
- `-u`   username (esempio: `admin`)
- `-p`   password (esempio: `123456`)

L'endpoint corretto è rintracciabile dalla documentazione delle api dell'istanza (ad esempio: `https://example.com/openapi/doc`)

### Esempio
Dato ad esempio questo set di righe (vedi esempio.csv):

| cig           | codice_fiscale_piva_proponente | denominazione_proponente | data_inizio | data_ultimazione | oggetto                                 | invitati.ragione_sociale           | invitati.identificativo_fiscale_estero | invitati.codice_fiscale | invitati.id_gruppo | invitati.ruolo | importo_gara | importo_aggiudicazione | scelta_contraente      | partecipanti | aggiudicatari | importo_somme_liquidate |
|---------------|--------------------------------|--------------------------|-------------|------------------|-----------------------------------------|------------------------------------|----------------------------------------|-------------------------|--------------------|----------------|--------------|------------------------|------------------------|--------------|---------------|-------------------------|
| CIGEXAMPLE001 | 1234567890                     | Comune di Bugliano       | 01/12/2023  | 31/12/2023       | Servizi di pulizia                      | Acme Italia spa                    |                                        | 13366030123             | ACME RTI           | Capogruppo     | 96546,91     | 73765,68               | 01-PROCEDURA APERTA    | 2            | 2             |                         |
| CIGEXAMPLE001 | 1234567890                     | Comune di Bugliano       | 01/12/2023  | 31/12/2023       | Servizi di pulizia                      | Future Società Cooperativa Sociale |                                        | 2007550123              | ACME RTI           | Associata      | 96546,91     | 73765,68               | 01-PROCEDURA APERTA    | 2            | 2             | 201341,83               |
| CIGEXAMPLE001 | 1234567890                     | Comune di Bugliano       | 01/12/2023  | 31/12/2023       | Servizi di pulizia                      | Politeia scs                       |                                        | 1915930123              |                    |                | 96546,91     | 73765,68               | 01-PROCEDURA APERTA    | 2            | 1             |                         |
| CIGEXAMPLE001 | 1234567890                     | Comune di Bugliano       | 01/12/2023  | 31/12/2023       | Servizi di pulizia                      | Amerigo soc coop ONLUS             |                                        | 5206930123              |                    |                | 96546,91     | 73765,68               | 01-PROCEDURA APERTA    | 2            | 1             |                         |
| CIGEXAMPLE001 | 1234567890                     | Comune di Bugliano       | 01/12/2023  | 31/12/2023       | Servizi di pulizia                      | Insieme soc coop sociale           |                                        | 12018841123             |                    |                | 96546,91     | 73765,68               | 01-PROCEDURA APERTA    | 2            | 1             |                         |
| CIGEXAMPLE001 | 1234567890                     | Comune di Bugliano       | 01/12/2023  | 31/12/2023       | Servizi di pulizia                      | Social Trust soc coop              |                                        | 2845680123              |                    |                | 96546,91     | 73765,68               | 01-PROCEDURA APERTA    | 2            | 1             |                         |
| CIGEXAMPLE001 | 1234567890                     | Comune di Bugliano       | 01/12/2023  | 31/12/2023       | Servizi di pulizia                      | Lucilla Società coop sociale       |                                        | 1186250123              |                    |                | 96546,91     | 73765,68               | 01-PROCEDURA APERTA    | 2            | 1             |                         |
| CIGEXAMPLE002 | 1234567890                     | Comune di Bugliano       | 01/03/2023  | 30/06/2023       | Fornitura cuffie con microfono e webcam | Pace Comupter Snc                  | ATU64462123                            |                         |                    |                | 1600,00      | 1539,00                | 02-PROCEDURA RISTRETTA | 2            | 2             | 311154,00               |
| CIGEXAMPLE002 | 1234567890                     | Comune di Bugliano       | 01/03/2023  | 30/06/2023       | Fornitura cuffie con microfono e webcam | MICROTEC Srl                       | DE813331123                            |                         |                    |                | 1600,00      | 1539,00                | 02-PROCEDURA RISTRETTA | 2            | 1             |                         |

Lo script invierà all'istanza target questi payload:

```json
{
    "invitati": [
        {
            "index": 0,
            "codice_fiscale": "13366030123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Acme Italia spa",
            "id_gruppo": "ACME RTI",
            "ruolo": "Capogruppo"
        },
        {
            "index": 1,
            "codice_fiscale": "2007550123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Future Società Cooperativa Sociale",
            "id_gruppo": "ACME RTI",
            "ruolo": "Associata"
        },
        {
            "index": 2,
            "codice_fiscale": "1915930123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Politeia scs",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 3,
            "codice_fiscale": "5206930123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Amerigo soc coop ONLUS",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 4,
            "codice_fiscale": "12018841123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Insieme soc coop sociale",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 5,
            "codice_fiscale": "2845680123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Social Trust soc coop",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 6,
            "codice_fiscale": "1186250123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Lucilla Società coop sociale",
            "id_gruppo": "",
            "ruolo": ""
        }
    ],
    "aggiudicatari": [
        {
            "index": 0,
            "codice_fiscale": "13366030123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Acme Italia spa",
            "id_gruppo": "ACME RTI",
            "ruolo": "Capogruppo"
        },
        {
            "index": 1,
            "codice_fiscale": "2007550123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Future Società Cooperativa Sociale",
            "id_gruppo": "ACME RTI",
            "ruolo": "Associata"
        }
    ],
    "partecipanti": [
        {
            "index": 0,
            "codice_fiscale": "13366030123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Acme Italia spa",
            "id_gruppo": "ACME RTI",
            "ruolo": "Capogruppo"
        },
        {
            "index": 1,
            "codice_fiscale": "2007550123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Future Società Cooperativa Sociale",
            "id_gruppo": "ACME RTI",
            "ruolo": "Associata"
        },
        {
            "index": 2,
            "codice_fiscale": "1915930123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Politeia scs",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 3,
            "codice_fiscale": "5206930123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Amerigo soc coop ONLUS",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 4,
            "codice_fiscale": "12018841123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Insieme soc coop sociale",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 5,
            "codice_fiscale": "2845680123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Social Trust soc coop",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 6,
            "codice_fiscale": "1186250123",
            "identificativo_fiscale_estero": "",
            "ragione_sociale": "Lucilla Società coop sociale",
            "id_gruppo": "",
            "ruolo": ""
        }
    ],
    "id": "CIGEXAMPLE001",
    "cig": "CIGEXAMPLE001",
    "codice_fiscale_piva_proponente": "1234567890",
    "denominazione_proponente": "Comune di Bugliano",
    "oggetto": "Servizi di pulizia",
    "scelta_contraente": "01-PROCEDURA APERTA",
    "importo_gara": 96546.91,
    "importo_aggiudicazione": 73765.68,
    "importo_somme_liquidate": false,
    "data_inizio": "2023-12-01T00:00:00+01:00",
    "data_ultimazione": "2023-12-31T00:00:00+01:00"
}
```


```json
{
    "invitati": [
        {
            "index": 0,
            "codice_fiscale": "",
            "identificativo_fiscale_estero": "ATU64462123",
            "ragione_sociale": "Pace Comupter Snc",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 1,
            "codice_fiscale": "",
            "identificativo_fiscale_estero": "DE813331123",
            "ragione_sociale": "MICROTEC Srl",
            "id_gruppo": "",
            "ruolo": ""
        }
    ],
    "aggiudicatari": [
        {
            "index": 0,
            "codice_fiscale": "",
            "identificativo_fiscale_estero": "ATU64462123",
            "ragione_sociale": "Pace Comupter Snc",
            "id_gruppo": "",
            "ruolo": ""
        }
    ],
    "partecipanti": [
        {
            "index": 0,
            "codice_fiscale": "",
            "identificativo_fiscale_estero": "ATU64462123",
            "ragione_sociale": "Pace Comupter Snc",
            "id_gruppo": "",
            "ruolo": ""
        },
        {
            "index": 1,
            "codice_fiscale": "",
            "identificativo_fiscale_estero": "DE813331123",
            "ragione_sociale": "MICROTEC Srl",
            "id_gruppo": "",
            "ruolo": ""
        }
    ],
    "id": "CIGEXAMPLE002",
    "cig": "CIGEXAMPLE002",
    "codice_fiscale_piva_proponente": "1234567890",
    "denominazione_proponente": "Comune di Bugliano",
    "oggetto": "Fornitura cuffie con microfono e webcam",
    "scelta_contraente": "02-PROCEDURA RISTRETTA",
    "importo_gara": 1600,
    "importo_aggiudicazione": 1539,
    "importo_somme_liquidate": false,
    "data_inizio": "2023-03-01T00:00:00+01:00",
    "data_ultimazione": "2023-06-30T00:00:00+02:00"
}
```