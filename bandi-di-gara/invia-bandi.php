<?php

if (!function_exists('curl_init')) {
    echo "ext-curl is required" . PHP_EOL;
    exit;
}

if (!function_exists('json_decode')) {
    echo "ext-json is required" . PHP_EOL;
    exit;
}

ini_set("display_errors", "1");

$options = getopt("hf:t:u:p:");

if (isset($options['h'])) {
    $help = implode(PHP_EOL, [
        ' -h         visualizza questo messaggio',
        ' -f=VALUE   percorso al file csv',
        ' -t=VALUE   target endpoint (esempio https://bugliano-qa.localtest.me/api/openapi/bandi-di-gara/2023)',
        ' -u=VALUE   target username (ex: admin)',
        ' -p=VALUE   target cms password (ex: password)',
    ]);
    echo "
Invia lotti alla sezione della trasparenza indicata

Esempio: 
docker container run --rm -v $(pwd):/app/ php:7.4-cli php /app/invia-bandi.php -flotto.csv -t=https://example.com/api/openapi/bandi-di-gara/2023 -u=admin -p=password

Opzioni:
$help
";
    exit(0);
}

// controllo opzioni
$sourceFile = $options['f'] ?? die('Opzione -f non specificata');
if (!file_exists($sourceFile)) {
    $sourceFile = __DIR__ . '/' . $sourceFile;
}
$remoteTarget = $options['t'] ?? die('Opzione -t non specificata');
$user = $options['u'] ?? die('Opzione -u non specificata');
$password = $options['p'] ?? die('Opzione -p non specificata');
$credentials = $options['u'] . ':' . $options['p'];

$contextOptions = stream_context_create([
    "ssl" => [
        "verify_peer" => false,
        "verify_peer_name" => false,
    ],
]);

function validate($headers)
{
    $row = array_fill_keys($headers, true);
    return isset(
        $row['cig'],
        $row['codice_fiscale_piva_proponente'],
        $row['denominazione_proponente'],
        $row['data_inizio'],
        $row['data_ultimazione'],
        $row['oggetto'],
        $row['invitati.ragione_sociale'],
        $row['invitati.identificativo_fiscale_estero'],
        $row['invitati.codice_fiscale'],
        $row['invitati.id_gruppo'],
        $row['invitati.ruolo'],
        $row['importo_gara'],
        $row['importo_aggiudicazione'],
        $row['scelta_contraente'],
        $row['partecipanti'],
        $row['aggiudicatari'],
        $row['importo_somme_liquidate']
    );
}

try {
    // lettura del file csv
    $csvFile = @fopen($sourceFile, 'r');
    $headers = [];
    $i = 0;
    while ($data = fgetcsv($csvFile, 100000, ';', '"')) {
        if ($i == 0) // First line, handle headers
        {
            $headers = $data;
            if (!validate($headers)) {
                throw new Exception('Le intestazioni del file csv non corrispondono al valore atteso');
            }
            $i++;
            unset($data);
            continue;
        }
        $row = [];
        for ($j = 0, $jMax = count($headers); $j < $jMax; ++$j) {
            $row[$headers[$j]] = $data[$j];
        }
        $rows[] = $row;
        $i++;
    }

    if (empty($rows)) {
        throw new Exception('Il file csv non contiene righe');
    }

    function formatPrice($data)
    {
        if ($data == '0' || $data == '') {
            return false;
        } else {
            return (float)str_replace(',', '.', $data);
        }
    }

    // generazione dei payload
    $payloads = [];
    foreach ($rows as $row) {
        $cig = $row['cig'];
        if (!isset($payloads[$cig])) {
            $payloads[$cig] = [
                'invitati' => [],
                'aggiudicatari' => [],
                'partecipanti' => [],
            ];
        }
        $payloads[$cig] = array_merge($payloads[$cig], [
            'id' => $cig,
            'cig' => $cig,
            'codice_fiscale_piva_proponente' => $row['codice_fiscale_piva_proponente'],
            'denominazione_proponente' => $row['denominazione_proponente'],
            'oggetto' => $row['oggetto'],
            'scelta_contraente' => $row['scelta_contraente'],
            'importo_gara' => formatPrice($row['importo_gara']),
            'importo_aggiudicazione' => formatPrice($row['importo_aggiudicazione']),
            'importo_somme_liquidate' => formatPrice($row['importo_somme_liquidate']),
            'data_inizio' => DateTime::createFromFormat(
                'd/m/Y',
                $row['data_inizio'],
                new DateTimeZone('Europe/Rome')
            )->setTime(0, 0)->format('c'),
            'data_ultimazione' => DateTime::createFromFormat(
                'd/m/Y',
                $row['data_ultimazione'],
                new DateTimeZone('Europe/Rome')
            )->setTime(0, 0)->format('c'),
        ]);

        $payloads[$cig]['invitati'][] = [
            'index' => count($payloads[$cig]['invitati']),
            'codice_fiscale' => $row['invitati.codice_fiscale'],
            'identificativo_fiscale_estero' => $row['invitati.identificativo_fiscale_estero'],
            'ragione_sociale' => $row['invitati.ragione_sociale'],
            'id_gruppo' => $row['invitati.id_gruppo'],
            'ruolo' => $row['invitati.ruolo'],
        ];
        if ($row['partecipanti'] == '2') {
            $payloads[$cig]['partecipanti'][] = [
                'index' => count($payloads[$cig]['partecipanti']),
                'codice_fiscale' => $row['invitati.codice_fiscale'],
                'identificativo_fiscale_estero' => $row['invitati.identificativo_fiscale_estero'],
                'ragione_sociale' => $row['invitati.ragione_sociale'],
                'id_gruppo' => $row['invitati.id_gruppo'],
                'ruolo' => $row['invitati.ruolo'],
            ];
        }
        if ($row['aggiudicatari'] == '2') {
            $payloads[$cig]['aggiudicatari'][] = [
                'index' => count($payloads[$cig]['aggiudicatari']),
                'codice_fiscale' => $row['invitati.codice_fiscale'],
                'identificativo_fiscale_estero' => $row['invitati.identificativo_fiscale_estero'],
                'ragione_sociale' => $row['invitati.ragione_sociale'],
                'id_gruppo' => $row['invitati.id_gruppo'],
                'ruolo' => $row['invitati.ruolo'],
            ];
        }
    }

    $host = parse_url($remoteTarget, PHP_URL_HOST);
    $servers = (array)gethostbynamel($host);
    $isLocalHost = in_array('127.0.0.1', $servers);

    $requestHeaders = [
        "Authorization: Basic " . base64_encode($credentials),
        "Content-Type: application/json",
    ];

    if ($isLocalHost) {
        $requestHeaders[] = "Host: $host";
        $remoteTarget = str_replace(['https://' . $host, 'http://' . $host], 'localhost', $remoteTarget);
    }

    foreach ($payloads as $cig => $payload) {
        try {
            $ch = curl_init();
            curl_setopt_array($ch, [
                CURLOPT_URL => $remoteTarget,
                CURLINFO_HEADER_OUT => 1,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_HEADER => 1,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POSTFIELDS => json_encode($payload),
                CURLOPT_HTTPHEADER => $requestHeaders,
            ]);

            $data = curl_exec($ch);
            if ($data === false) {
                $errorCode = curl_errno($ch) * -1;
                $errorMessage = curl_error($ch);
                curl_close($ch);
                throw new \Exception($cig . ' - ' . $errorMessage, $errorCode);
            }
            $info = curl_getinfo($ch);
            curl_close($ch);

            $headers = substr($data, 0, $info['header_size']);
            if ($info['download_content_length'] > 0) {
                $body = substr($data, -$info['download_content_length']);
            } else {
                $body = substr($data, $info['header_size']);
            }

            $response = json_decode($body);

            if (isset($response->error_message)) {
                $errorMessage = '';
                if (isset($response->error_type)) {
                    $errorMessage = $response->error_type . ': ';
                }
                $errorMessage .= $response->error_message;
                throw new \Exception($cig . ' - ' . $errorMessage);
            }

            if ($info['http_code'] == 401) {
                throw new \Exception($cig . ' - Authorization Required');
            }
            if (!in_array($info['http_code'], [100, 200, 201, 202])) {
                throw new \Exception("$cig . ' - Request return status " . $info['http_code']);
            }

            echo '[SUCCESS] ' . $cig . PHP_EOL;

        } catch (Throwable $e) {
            echo '[ERROR] ' . $e->getMessage() . PHP_EOL;
        }
    }
} catch (Throwable $e) {
    echo '[ERROR] ' . $e->getMessage() . PHP_EOL;
}