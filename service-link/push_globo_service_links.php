<?php

if (!function_exists('curl_init')) {
    echo "ext-curl is required" . PHP_EOL;
    exit;
}

if (!function_exists('json_decode')) {
    echo "ext-json is required" . PHP_EOL;
    exit;
}

$options = getopt("hs:t:u:p:");

if (isset($options['h'])) {
    $help = implode(PHP_EOL, [
        ' -h         visualizza questo messaggio',
        ' -s=VALUE   source endpoint api (ex: https://sportellotelematico.comune.trani.bt.it/rest/pnrr/procedures)',
        ' -t=VALUE   opencity cms target (ex: https://www.comune-qa.bugliano.pi.it/)',
        ' -u=VALUE   opencity cms username (ex: admin)',
        ' -p=VALUE   opencity cms password (ex: password)',
    ]);
    echo "
Crea o aggiorna il catalogo dei servizi del sito opencity cms target dal catalogo esposto da api pubbliche Maggioli/Globo.

Example: 

php push_globo_service_links.php -shttps://sportellotelematico.comune.trani.bt.it/rest/pnrr/procedures -t=https://www.comune-qa.bugliano.pi.it/ -u=admin -p=password

docker container run --rm -v $(pwd):/app/ php:7.4-cli php /app/push_globo_service_links.php -shttps://sportellotelematico.comune.trani.bt.it/rest/pnrr/procedures -t=https://www.comune-qa.bugliano.pi.it/ -u=admin -p=password

Options:
$help
";
    exit(0);
}

// check and format script options
$sourcePath = $options['s'] ?? die('Missing -s option');
$sourceData = json_decode(@file_get_contents($sourcePath), true);
if (empty($sourceData)) {
    die(sprintf('Source data not found in %s', $sourcePath));
}
$remoteTarget = $options['t'] ?? die('Missing -t option');
$remoteTarget = rtrim($remoteTarget, '/');
$user = $options['u'] ?? die('Missing -u option');
$password = $options['p'] ?? die('Missing -p option');
$credentials = $options['u'] . ':' . $options['p'];

// call without ssl check by default
$contextOptions = stream_context_create([
    "ssl" => [
        "verify_peer" => false,
        "verify_peer_name" => false,
    ],
]);

$apiBaseUrl = $remoteTarget . '/api/opendata/v2/content';

// find service container location
$serviceContainer = json_decode(
    @file_get_contents($apiBaseUrl . '/read/all-services', false, $contextOptions),
    true
);
$serviceContainerNodeId = $serviceContainer['metadata']['mainNodeId'] ?? false;
if (empty($serviceContainerNodeId)) {
    die(sprintf('Remote target not found in %s', $remoteTarget));
}

// find topic name -> remote_id map
$topicsMap = json_decode(
    @file_get_contents($apiBaseUrl . '/search/' . urlencode('select-fields [data.name=>metadata.remoteId] classes [topic] sort [name=>asc] limit 200'), false, $contextOptions),
    true
);

// loop items
$count = count($sourceData);
foreach ($sourceData as $index => $item) {
    $contentPayload = [
        'type' => $item['categoria'],
        'name' => html_entity_decode($item['nome'], ENT_QUOTES),
        'identifier' => $item['urn'],
        'abstract' => html_entity_decode($item['descrizione_breve'], ENT_QUOTES),
        'location' => $item['url'] . '|Vai alla scheda del servizio',
    ];
    $topics = [];
    foreach ($item['argomenti'] as $topicName){
        if (isset($topicsMap[$topicName])){
            $topics[] = $topicsMap[$topicName];
        }
    }
    if (!empty($topics)){
        $contentPayload['topics'] = $topics;
    }
    $payload = [
        'metadata' => [
            'classIdentifier' => 'public_service_link',
            'remoteId' => 'globo_' . md5($item['urn']),
            'parentNodes' => [$serviceContainerNodeId],
        ],
        'data' => $contentPayload,
    ];

    // upsert payload
    echo ++$index . '/' . $count . ' -> Push item ' . $contentPayload['identifier'] . PHP_EOL;
    echo "      " . $contentPayload['name'] . PHP_EOL;
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_URL => $apiBaseUrl . "/create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POSTFIELDS => json_encode($payload),
        CURLOPT_HTTPHEADER => [
            "Authorization: Basic " . base64_encode($credentials),
            "Content-Type: application/json",
        ],
    ]);
    $response = curl_exec($curl);
    $error = curl_error($curl);
    curl_close($curl);
    $responseData = json_decode($response, true);
    if ($error) {
        echo "      [error] cURL Error #:" . $error . PHP_EOL;
    } elseif (isset($responseData['error_message'])) {
        echo "      [error] " . $responseData['error_message'] . PHP_EOL;
    } else {
        $message = $responseData['result']['message'];
        $method = $responseData['result']['method'];
        echo "      [$message] $method item in $remoteTarget/content/view/full/" . $responseData['result']['content']['metadata']['mainNodeId'] . PHP_EOL;
    }
}

exit(0);