# Crea link a schede servizio esterne tramite l'esecuzione dello script `push_globo_service_links`

Crea o aggiorna il catalogo dei servizi del sito opencity cms target dal catalogo esposto da api pubbliche Maggioli/Globo.

### Example:
```
php push_globo_service_links.php \
-shttps://sportellotelematico.comune.trani.bt.it/rest/pnrr/procedures \
-t=https://www.comune-qa.bugliano.pi.it/ \
-u=admin \
-p=password
```

```
docker container run --rm -v $(pwd):/app/ php:7.4-cli php /app/push_globo_service_links.php \
-shttps://sportellotelematico.comune.trani.bt.it/rest/pnrr/procedures \
-t=https://www.comune-qa.bugliano.pi.it/ \
-u=admin \
-p=password
```

### Options:
| Option   | Descrizione                                                                                     |
|----------|-------------------------------------------------------------------------------------------------|
| -h       | visualizza questo messaggio                                                                     |
| -s=VALUE | source endpoint api (ex: https://sportellotelematico.comune.trani.bt.it/rest/pnrr/procedures)   |
| -t=VALUE | opencity cms target (ex: https://www.comune-qa.bugliano.pi.it/)                                 |
| -u=VALUE | opencity cms username (ex: admin)                                                               |
| -p=VALUE | opencity cms password (ex: password)                                                            |