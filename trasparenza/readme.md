# Utilizzo degli endpoint `/trasparenza/{page}`

L'alberatura della trasparenza viene appiattita negli endpoint `/trasparenza/{page}`

Per pubblicare un contenuto dentro a una pagina occorre quindi usare il parametro page della pagina in cui si vuole pubblicare (che viene restituito come campo delle pagine trasparenza).

I contenuti della singola pagina sono rintracciabili dalla pagina contenitore e dal loro id: `/trasparenza/{page}/{id}`

### Esempio creazione di un documento in "Manifestazione sportiva" 

`POST https://opencitydemo.openpa.opencontent.io/api/openapi/trasparenza/manifestazione_sportiva`

Payload:

```
{
   "resource_name":"Document",
   "id":"il-mio-doc",
   "name":"Bilancio consuntivo 2021",
   "document_type":[
      "Bilancio consuntivo"
   ],
   "topics":[
      {
         "uri":"https://opencitydemo.openpa.opencontent.io/api/openapi/argomenti/topic_1_governo_e_settore_pubblico#Politica"
      }
   ],
   "abstract":"Il bilancio del 2021",
   "has_organization":[
      {
         "uri":"https://opencitydemo.openpa.opencontent.io/api/openapi/amministrazione/uffici/7527419b2d5fde514875e35da20bfe1e#Ufficio-relazioni-con-il-pubblico"
      }
   ],
   "license":[
      "Creative Commons Attribution 4.0 International (CC BY 4.0)"
   ],
   "format":[
      "pdf"
   ],
   "start_time":"2021-12-31",
   "publication_start_time":"2021-12-31",
   "attachments":[
      {
         "filename":"bilancio_2021.pdf",
         "uri":"https://opencitydemo.openpa.opencontent.io/content/download/448/7751/file/DeterminadiImpegno_Originale_7_2024.pdf"
      }
   ]
}
```

### Esempio creazione di una pagina trasparenza in Atti di concessione

`POST "https://opencitydemo.openpa.opencontent.io/api/openapi/trasparenza/atti_di_concessione"`

Payload:

```
{
   "resource_name":"PaginaTrasparenza",
   "id":"esempio",
   "titolo":"Esempio pagina"
}
```

Response:

```
{
   "id":"esempio",
   "uri":"https://opencitydemo.openpa.opencontent.io/api/openapi/trasparenza/atti_di_concessione/esempio#Esempio-pagina",
   "published_at":"2024-02-28T21:52:55+01:00",
   "modified_at":"2024-02-28T21:52:55+01:00",
   "is_public":true,
   "page":"esempio_pagina",
   "titolo":"Esempio pagina",
   ...
}
```


### Esempio creazione di un link dentro alla pagina "Esempio pagina"

`POST "https://opencitydemo.openpa.opencontent.io/api/openapi/trasparenza/esempio_pagina"`

Payload:

```
{
   "resource_name":"Link",
   "id":"google",
   "name":"Link a Google",
   "location":"https://www.google.com"
}
```